/*
	Basado en el ejemplo WebClient de Arduino.
	Recomiendo echar un vistazo a la libreria https://github.com/esp8266/Arduino/blob/master/libraries/ESP8266WiFi/src/WiFiUdp.h
*/


//Librerias wifi
#include <SPI.h>
#include <WiFi.h>
#include <WiFiUdp.h>

// Librerias de DHT
#include "DHT.h" 
#define DHTPIN 2 
#define DHTTYPE DHT11
DHT dht(DHTPIN, DHTTYPE); 

/*
// Variables de la conexion NTP
unsigned int localPort = 2390;      // local port to listen for UDP packets
IPAddress timeServer(129, 6, 15, 28); // time.nist.gov NTP server
const int NTP_PACKET_SIZE = 48; // NTP time stamp is in the first 48 bytes of the message
byte packetBuffer[ NTP_PACKET_SIZE]; //buffer to hold incoming and outgoing packets
*/

// Variables de la conexion al AP. Sus valores se deben substituir en función de las propiedades de la red a la qual nos conectamos
char ssid[] = "MOVISTAR_0FD1_EXT"; //  your network SSID (name)
char pass[] = "51534c3d4a4a765372302824417a79406377272b384b4c6c3f24396e712c4d5";    // your network password (use for WPA, or use as key for WEP)
//int keyIndex = 0;            // your network key Index number (needed only for WEP)
int status = WL_IDLE_STATUS;

// Variables del programa
String hola = "Hola num ";
String res;

//Tupla que contiene los campos del paquete.
//Formato de paquete [id, timestamp, flags, humedad, temperatura, end] = 1 + 4 + 1 + 4 + 4 + 1 bytes = 15 bytes
struct {
	byte id = 222; //222 por poner algo
	unsigned long timestamp;
	byte flags;
	float humedad;
	float temperatura;
	byte end = 0; //Valor en bytes del codigo ASCII del string '\0' (NUL)
} paquete;
int size_paquete = 15;

// Variables de la conexion al server del AP

/* Dirección IP del servidor al cual enviamos el paquete.
En caso de querer usar una dirección DDNS en lugar de IP estática, simplemente
comentamos la siguiente linea y comentamos la que está debajo de la siguiente.*/
IPAddress server(192, 168, 1, 132); // IP númerica de Claudio (no DNS)
//char server[] = "www.google.com";    // name address for Google (using DNS)

/*Inicializamos la libreria de cliente Ethernet con la dirección IP
y puerto del servidor*/
WiFiUDP UdpNTP, Udp;

void setup() {
	//Inicializa el sensor de DHT
	Serial.begin(9600); //Se inicia la comunicación serial 
	dht.begin(); //Se inicia el sensor

	// Todo el código restante de la función setup() está sacado del ejemplo WebClient de Arduino

	//Initialize serial and wait for port to open:
	while (!Serial) {
		; // wait for serial port to connect. Needed for native USB port only
	}  // check for the presence of the shield:
	if (WiFi.status() == WL_NO_SHIELD) {
		Serial.println("WiFi shield not present");
		// don't continue:
		while (true);
	}

	String fv = WiFi.firmwareVersion();
	if (fv != "1.1.0") {
		Serial.println("Please upgrade the firmware");
	}

	// attempt to connect to Wifi network:
	while (status != WL_CONNECTED) {
		Serial.print("Attempting to connect to SSID: ");
		Serial.println(ssid);
		// Connect to WPA/WPA2 network. Change this line if using open or WEP network:
		status = WiFi.begin(ssid, pass);

		// wait 10 seconds for connection:
		delay(6000);
	}
	Serial.println("Connected to wifi");
	printWifiStatus();

	Serial.println("\nComenzando la transmision de datos...");
	// if you get a connection, report back via serial:
}

void loop() {
	//Formamos el paquete. Formato = [id, timestamp, flags, humedad, temperatura, end] = 1 + 4 + 1 + 4 + 4 + 1 bytes = 15 bytes
	paquete.timestamp = random(408988800UL, 608988800UL);
	paquete.flags = 69; // 69 en decimal = 01000101 en binario. El 69 es por poner algo. No tiene sentido ya que aun se debe acordar entre los dos equipos y documentar este campo 

	//float h = dht.readHumidity(); //se lee la humedad
	//float t = dht.readTemperature(); // se lee la temperatura
	//Humedad y temperatura random. Si se tiene el sensor de Arduino, quitar estas lineas de abajo y descomentar las de arriba	
	paquete.humedad = random(22.0, 70.0); 
	paquete.temperatura = random(18.0, 24.0);

	// Generamos el paquete con la información que acababos de obtener o generar
	byte bytes_paquete [size_paquete];
	formarPaquete(&bytes_paquete[0]);

	//Enviamos el paquete al servidor
	Udp.beginPacket(server, 55555);
	Udp.write(bytes_paquete, size_paquete);
	Serial.print("ID: ");
	Serial.print(paquete.id);
	Serial.print(" | Timestamp: ");
	Serial.print(paquete.timestamp);
	Serial.print(" | Flags: ");
	Serial.print(paquete.flags);
	Serial.print(" | Humedad: ");
	Serial.print(paquete.humedad);
	Serial.print(" | Temperatura: "); 
	Serial.println(paquete.temperatura);
	Udp.endPacket();
	// Esperamos 1 segundo antes de volver a generar otro paquete
	delay(2000);
}

void formarPaquete(byte *bytes_paquete) {
	bytes_paquete[0] = paquete.id;
	byte cuatrobytes [4];

	// La función memcpy copia el valor de una cantidad determinada de bytes de una variable a otra
	// Cabecera de la función memcpy:
	// void * memcpy ( void * destination, const void * source, size_t num );

	memcpy(cuatrobytes, &paquete.timestamp, 4);
	bytes_paquete[1] = cuatrobytes[0];
	bytes_paquete[2] = cuatrobytes[1];
	bytes_paquete[3] = cuatrobytes[2];
	bytes_paquete[4] = cuatrobytes[3];

	bytes_paquete[5] = paquete.flags;

	memcpy(cuatrobytes, &paquete.humedad, 4);
	bytes_paquete[6] = cuatrobytes[0];
	bytes_paquete[7] = cuatrobytes[1];
	bytes_paquete[8] = cuatrobytes[2];
	bytes_paquete[9] = cuatrobytes[3];

	memcpy(cuatrobytes, &paquete.temperatura, 4);
	bytes_paquete[10] = cuatrobytes[0];
	bytes_paquete[11] = cuatrobytes[1];
	bytes_paquete[12] = cuatrobytes[2];
	bytes_paquete[13] = cuatrobytes[3];

	bytes_paquete[14] = paquete.end;
}

void printWifiStatus() {
	// print the SSID of the network you're attached to:
	Serial.print("SSID: ");
	Serial.println(WiFi.SSID());

	// print your WiFi shield's IP address:
	IPAddress ip = WiFi.localIP();
	Serial.print("IP Address: ");
	Serial.println(ip);

	// print the received signal strength:
	long rssi = WiFi.RSSI();
	Serial.print("signal strength (RSSI):");
	Serial.print(rssi);
	Serial.println(" dBm");
}
