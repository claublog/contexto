# -- coding: utf-8 --
import socket
import struct

# Programa encargado de escuchar por un puerto UDP e imprimir los campos interpretandolo como se enviee en el programa "EnviarTempHum.ino"
# TODAVIA NO ESTÁ TERMINADO!! por lo tanto no funciona como deberia


# Por defecto cuando se lee un byte se omiten los ceros de la izquierda (por ejemplo, "1010")
# Esta funcion se encarga de añadir caracteres de cero a la izquierda del string (en el caso anterior, quedaria "00001010")
def anadir_ceros (inf_cruda) :
	res = [""] * len(inf_cruda)
	i = 0
	for palabra in inf_cruda :
		count = len(palabra)
		res[i] = palabra
		# añade ceros hasta llegar a 8 caracteres
		while (count < 8) :
			res[i] = "0" + res[i]
			count += 1
		i += 1
	return res

def bin2float (byte0, byte1, byte2, byte3) :
	str_quad = [None] * 4
	str_quad[0] = int(byte0, 2)
	str_quad[1] = int(byte1, 2)
	str_quad[2] = int(byte2, 2)
	str_quad[3] = int(byte3, 2)
	binario = struct.pack('4B', *str_quad)
	return struct.unpack('>f', binario)[0]

# Crea un socket UDP preparado para recibir conexiones entrantes por el puerto 55555
s = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
s.bind((socket.gethostbyname(socket.gethostname()), 55555))
print "Escuchando por el puerto 55555"
while True:
	# Esperamos a recibir un paquete. Establecemos un buffer de hasta 1024 bytes por paquete
	data, addr = s.recvfrom(1024)
	# Substituye los caracteres 'b' por strings
	inf_cruda = (' '.join(format(ord(x), 'b') for x in data)).split(' ')
	msj = anadir_ceros(inf_cruda)
	
	# Obtenemos el ID, que corresponde al primer byte
	identif = int(msj[0], 2)

	# Obtenemos el timestamp, que corresponden a los siguientes 4 bytes
	str_quad = msj[4] + msj[3] + msj[2] + msj[1]
	timestamp = int(str_quad, 2)

	# Obtenemos los flags, que sera el sexto byte
	flags = msj[5]

	# Obtenemos la humedad, que son los siguientes 4 bytes
	humedad = bin2float (msj[9], msj[8], msj[7], msj[6])

	# Obtenemos la temperatura, que son los siguientes 4 bytes
	temperatura = bin2float (msj[13], msj[12], msj[11], msj[10])

	# Imprime el mensaje con los ceros añadidos
	print "IP Origen: {0} | Puerto Origen: {1}".format(addr[0], addr[1]),
	print "| ID: {0}".format(identif),
	print "| Timestamp: {0}".format(timestamp),
	print "| Flags: {0}".format(flags),
	print "| Humedad: {0}".format(humedad),
	print "| Temperatura: {0}".format(temperatura)
